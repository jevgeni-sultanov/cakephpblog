<?php
namespace App\Model\Table;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ArticlesTable extends Table
{
    /**
     * @param array $config
     * @return void
     */
    public function initialize(array $config): void
    {
        $this->addBehavior('Timestamp');
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->notBlank('title')
            ->requirePresence('title')
            ->notBlank('body')
            ->requirePresence('body')
            ->add('reference', [
                'unique' => [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                ]
            ])
            ->allowEmptyString('reference');

        return $validator;
    }

    /**
     * Get related articles with a matching title
     *
     * @param Entity $article
     * @return Query
     */
    public function getRelated(Entity $article): Query
    {
        return $this->find()->where(function (QueryExpression $expression) use ($article) {
            return $expression
                ->like('title', "%{$article->title}%")
                ->not(['id' => $article->id]);
        });
    }
}

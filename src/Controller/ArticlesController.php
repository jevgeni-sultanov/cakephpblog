<?php
namespace App\Controller;

use Cake\Http\Response;

class ArticlesController extends AppController
{
    /**
     * @throws \Exception
     * @return void
     */
    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Flash');
    }

    /**
     * @return void
     */
    public function index(): void
    {
        $this->set('articles', $this->Articles->find('all'));
    }

    /**
     * @param string $id
     * @return void
     */
    public function view(string $id): void
    {
        $article = $this->Articles->get(h($id));

        $this->set([
            'article' => $article,
            'relatedArticles' => $this->Articles->getRelated($article),
        ]);
    }

    /**
     * @return Response|null
     */
    public function add()
    {
        $article = $this->Articles->newEntity();

        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());

            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The reference exists in another article'));
        }
        $this->set('article', $article);
    }

    /**
     * @param string $id
     * @return Response|null
     */
    public function edit(string $id)
    {
        $article = $this->Articles->get(h($id));

        if ($this->request->is(['post', 'put'])) {
            $this->Articles->patchEntity($article, $this->request->getData());

            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to update your article.'));
        }

        $this->set([
            'article'         => $article,
            'relatedArticles' => $this->Articles->getRelated($article),
        ]);
    }

    /**
     * @param string $id
     * @return Response|null
     */
    public function delete(string $id)
    {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get(h($id));

        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The article with id: {0} has been deleted.', h($id)));

            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * @param string $id
     * @return Response|null
     */
    public function archive(string $id)
    {
        $article = $this->Articles->get(h($id));

        if ($this->request->is('post')) {
            $article->archived = true;

            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been archived.'));

                return $this->redirect(['action' => 'index']);
            }
        }
    }
}

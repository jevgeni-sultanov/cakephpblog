<?php $this->assign('title', 'Article preview') ?>

<div class="columns">
    <h1><?= h($article->title) ?></h1>
    <p><?= h($article->body) ?></p>
    <p>Reference: <?= $article->reference ?></p>
    <p>Created: <?= $article->created->format('d.m.Y H:i:s') ?></p>

    <?= $this->Form->postLink(
            'Archive',
            ['action' => 'archive', $article->id],
            ['class' => 'button'])
    ?>
</div>
<h1 class="columns">Related articles:</h1>
<table class="table">
    <?= $this->element('Table/head', ['noActions' => true]) ?>
    <?= $this->element('Table/body', ['dataObjects' => $relatedArticles, 'noActions' => true, 'noSelect' => true]) ?>
</table>

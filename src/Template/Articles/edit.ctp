<?php $this->assign('title', 'Edit article') ?>

<div class="columns">
    <?php
    echo $this->Form->create($article);
    echo $this->Form->control('title');
    echo $this->Form->control('body', ['rows' => '3']);
    echo $this->Form->control('reference');
    echo $this->Form->button(__('Save Article'));
    echo $this->Form->end();
    ?>
</div>

<h1 class="columns">Select related articles:</h1>
<table class="table">
    <?= $this->element('Table/head', ['noActions' => true]) ?>
    <?= $this->element('Table/body', ['dataObjects' => $relatedArticles, 'noActions' => true, 'noSelect' => false]) ?>
</table>

<?php $this->assign('title', 'Article list') ?>

<div class="columns">
    <?= $this->Html->link('Add Article', ['action' => 'add'], ['class' => 'button']) ?>
</div>
<table class="table">
    <?= $this->element('Table/head', ['noActions' => false]) ?>
    <?= $this->element('Table/body', ['dataObjects' => $articles, 'noActions' => false, 'noSelect' => false]) ?>
</table>

<?php

foreach ($dataObjects as $dataObject) {
    echo $this->Html->tableCells(
        [
            [
                $dataObject->id,
                !$noSelect ? $this->Html->link($dataObject->title, ['action' => 'view', $dataObject->id]) : $dataObject->title,
                $dataObject->reference,
                $dataObject->created->format('d.m.Y H:i:s'),
                !$noActions ? $this->element('Table/actions', ['dataObject' => $dataObject]) : '',
            ],
        ],
        $this->TableRow->applyArchivedStyle($dataObject->archived)
    );
}

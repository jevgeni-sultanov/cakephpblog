<?php

echo implode(
    ' ',
    [
        $this->Form->postLink(
            'Delete',
            ['action' => 'delete', $dataObject->id],
            ['confirm' => 'Are you sure?']
        ),
        $this->Html->link('Edit', ['action' => 'edit', $dataObject->id]),
    ]
);

<?php

echo $this->Html->tableHeaders(
    [
        'Id',
        'Title',
        'Reference',
        'Created',
        !$noActions ? 'Actions' : '',
    ]
);

<?php
namespace App\View\Helper;

use Cake\View\Helper;

class TableRowHelper extends Helper
{
    public const CLASS_NAME_ARCHIVED = 'archived';

    /**
     * @param bool $isArchived
     * @return array
     */
    public function applyArchivedStyle(bool $isArchived): array
    {
        return $isArchived ? ['class' => self::CLASS_NAME_ARCHIVED] : [];
    }
}

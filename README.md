# CakePHP Blog

A simple blog example developed with CakePHP framework

## Before start
Make sure all required packages are installed:
```bash
sudo apt install apache2 libapache2-mod-php
sudo apt install mysql-server
sudo apt install php php-mysql php-pdo-mysql php-curl php-gd php-pear php-imagick php-imap php-recode php-tidy php-xmlrpc php-mbstring php-intl
```
## Deploy Apache HTTP server
Create the new project directory:
```bash
sudo mkdir -p /var/www/cakephpblog.com/public_html
```
Grant user permissions:
```bash
sudo chown -R $USER:$USER /var/www/cakephpblog.com/public_html
```
Grant read access: 
```bash
sudo chmod -R 755 /var/www
```
Create the new virtual host file:
```bash
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/cakephpblog.com.conf
```
Open the file and paste data:
```bash
<VirtualHost *:80>
    ServerAdmin admin@cakephpblog.com
    ServerName cakephpblog.com
    ServerAlias www.cakephpblog.com
    DocumentRoot /var/www/cakephpblog.com/public_html/webroot
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
<Directory /var/www/cakephpblog.com/public_html/webroot>
    AllowOverride All
</Directory>
```
Enable the virtual host:
```bash
a2ensite cakephpblog.com.conf
```
Modify local hosts file:
```bash
sudo nano /etc/hosts
```
Put there:
```bash
127.0.0.1   cakephpblog.com
```
Restart Apache:
```bash
sudo systemctl restart apache2
```
## Deploy database
Login to a database shell:
```bash
sudo mysql -u root
 ```
Execute next queries:
```bash
CREATE USER 'cakephpblog'@'localhost' IDENTIFIED WITH mysql_native_password BY 'cakephpblog';
GRANT ALL PRIVILEGES ON *.* TO 'cakephpblog'@'localhost';
CREATE DATABASE blog CHARACTER SET utf8 COLLATE utf8_unicode_ci;
```
Restart MySQL:
```bash
sudo systemctl restart mysql
```
## Deploy project
Get the repository and put it to **/var/www**:

Via SSH:
```bash
git@gitlab.com:jevgeni-sultanov/cakephpblog.git
```
Via HTTPS:
```bash
https://gitlab.com/jevgeni-sultanov/cakephpblog.git
```
Go to the project folder `/var/www/cakephpblog.com/public_html/` and execute:
```bash
bin/cake migrations migrate
```
You are good to go! Follow the link to start:
```bash
http://cakephpblog.com/
```
